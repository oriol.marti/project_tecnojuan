<?php
$servername = "";
$username = "";
$password = "";

try {
  $conn = new PDO("mysql:host=$servername;dbname=oriolmarti7e5_productes", $username, $password);
  // set the PDO error mode to exception
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   /* echo "Connected successfully";  */
} catch(PDOException $e) {
  echo "Pagina fora de servei temporalment :(" . $e->getMessage();
}

?>
