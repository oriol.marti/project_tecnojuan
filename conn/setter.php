<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'conexion.php';
require_once '../public/model.php';
//require_once 'cataleg.php';

$laMevaSentencia = $conn->prepare("SELECT * FROM productes");


$laMevaSentencia->setFetchMode(PDO::FETCH_CLASS , 'Producte');

//5) Executo la sentencia
$laMevaSentencia->execute();
echo "Productos BDD";
//6) Itero per sobre cadascuns del gosos i els faig bordar
while($elmeuproducte = $laMevaSentencia->fetch()){
   echo "id: " . $elmeuproducte->id . "\n";
   echo "titol: " . $elmeuproducte->titol . "\n";
   echo "categoria: " . $elmeuproducte->categoria . "\n";
   echo "preu: " . $elmeuproducte->preu . "\n";
   echo "descripcio: " . $elmeuproducte->descripcio . "\n";
   echo "color: " . $elmeuproducte->color . "\n";
   echo "stoc: " . $elmeuproducte->stoc . "\n";
   echo "caracteristiques: " . $elmeuproducte->caracteristiques . "\n";
   echo "especificacions: " . $elmeuproducte->especificacions . "\n";
   echo "instruccions: " . $elmeuproducte->instruccions . "\n";
   echo "quantitat: " . $elmeuproducte->quantitat . "\n";
   echo "fotos: " . $elmeuproducte->fotos . "\n";
}
?>