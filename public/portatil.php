<?php
//$_SESSION["$laMevaCistella"];
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'model.php';
require_once 'cataleg.php';
session_start();
?>
<html>

<head>
	<title>TecnoJuan</title>
	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Pagina web G4">
	<meta name="author" content="Oriol, Arnau, Roger">
	<meta name="apple-mobile-web-app-title" content="TecnoJuan">

	<meta property="og:title" content="A Basic HTML5 Template">
	<meta property="og:type" content="website">
	<meta property="og:url" content="url">
	<meta property="og:description" content="Home page">
	<meta property="og:image" content="image.png">

	<link rel="icon" href="url">
	<link rel="icon" href="url" type="image/svg+xml">
	<link rel="apple-touch-icon" href="/image.png">

	<link rel="stylesheet" href="styles.css">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>

<body>
	<!-----------------MENU----------------->
	<?php
	require 'capcalera.php'
	?>
	<!-----------------MENU----------------->

    <div class="grid-container">
        
        
	<div class="grid-item"> 
		<a href="portatil.php"></a>
		<img id="img-news"  style="width: 1050px;" src="https://portatilgaming.net/wp-content/uploads/2020/07/portatil-gaming-1.jpg" alt="IMGNOT2">
		<h5 class="text-center"> ASUS ROG Zephyrus M16, análisis: Un portátil gaming brillante</h5>
		<p id="text-noticia"> El ASUS ROG Zephyrus M16 se presenta como un modelo superior al ASUS ROG Zephyrus G15, 
			un portátil gaming muy ligero que tuvimos la oportunidad de analizar recientemente en su configuración con 
			APU Ryzen 7 5800HS y tarjeta gráfica GeForce RTX 3060...
		</p>
		<a id="autor" >John McReary</a> <a id="fecha">21 - 10 - 2021</a>

    </div>
    </div>
    <main>
	</main>
	<br />&nbsp;
	<!-----------------FOOTER----------------->
	<?php
	require 'footer.php'
	?>
	<!-----------------FOOTER----------------->
</body>