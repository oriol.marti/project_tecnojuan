
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/1f7457abdb.js"></script>


<div id="blurry-filter"></div>

	<header>
		<div>
			<a href="index.php" id="title">
				<img class="logo_capcalera" style="width: 150px;height: 150px;" src="imagenes/logo_size_invert.png" alt="LOGOTECNOJUAN">
      </a>
			<article id="reference">
  <div class="offcanvas offcanvas-start" id="demo">
  <div class="offcanvas-header">
    <button type="button" class="btn-close" data-bs-dismiss="offcanvas"></button>
  </div>
  <div class="offcanvas-body ">
	<div id="">
    <p>
		<a href="productos.php">Productos</a>
    </p>
    <p>
		<a href="noticias.php">Noticias</a>
    </p>
    <p>
		<a href="contacte.php">Contacto</a>
    </p>
  </div>
  </div>
</div> 
      <button class="d-flex btn btn-danger m-1 d-sm-block d-md-none" type="button" data-bs-toggle="offcanvas" data-bs-target="#demo"><i class="fas fa-bars fa-2x"></i></button>
      <button type="button" class="d-flex btn btn-danger m-1" onclick="location.href = '../private/pagina1.php'"><i class="fa fa-user fa-2x" aria-hidden="true"></i></button>
      <button type="button" class="d-flex btn btn-danger m-1" onclick="show()"><i class="fas fa-shopping-cart fa-2x"></i></button>
			</article>
		</div>
	</header>
	<div id="folders">
		<a class="desktop_menu d-md-block d-sm-none" href="productos.php">Productos</a>
		<a class="desktop_menu d-md-block d-sm-none" href="noticias.php">Noticias</a>
		<a class="desktop_menu d-md-block d-sm-none" href="contacte.php">Contacto</a>
  </div>

  <div class="offcanvas offcanvas-start" id="demo">
  <div class="offcanvas-header">
    <button type="button" class="btn-close" data-bs-dismiss="offcanvas"></button>
  </div>
  <div class="offcanvas-body">
	<div id="">
		<a href="productos.php">Productos</a>
		<a href="noticias.php">Noticias</a>
		<a href="contacte.php">Contacto</a>

  </div>
  </div>
</div>



<div class="modal" id="myModal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cistella</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
            </div>
            <div class="modal-body">
            <?php

                if(isset($_SESSION['cistella'])){
                  $laMevaCistella = unserialize($_SESSION['cistella']);
                }
                echo "<table class='table'>
                <thead>
                  <tr>
                    <th scope='col'>Producto</th>
                    <th scope='col'>Nombre</th>
                    <th scope='col'>Precio</th>
                    <th scope='col'>Cantidad</th>
                  </tr>
                </thead>
                <tbody>";

                foreach ($laMevaCistella->productesCistella as $prod) {

                echo "
                
                
                  <tr>
                    <th scope='row'><img style='width: 100px' alt='IMG_PROD' src='" . $prod->foto1 . "'<div class='product_text'></th>
                    <td>" . $prod->titol  . "</td>
                    <td>" . $prod->preu . "€</td>
                    <td>" . $prod->quantitat . "</td>
                    <td>
                      <form class='m-0 h-100' method='POST' action='deleteProducte.php'>
                      <input type='hidden' name='idDelete' value='" . $prod->id . "'>
                      <button class='btn btn-danger btn-animated' type='submit'>eliminar del carro</button>
                      </form>
                    </td>
                  </tr>";
                }
                ?>
                <tfoot>
                  <tr>
                    <td><p class="ps-3 textmuted fw-bold h6 mb-0">TOTAL IMPORTE</p></td>
                    <td></td>
                    <td><?php echo $laMevaCistella->getTotal() . "€"; ?></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tfoot>
                </tbody>
              </table>            
            </div>
            <div class="modal-footer">
              <button type="button" onclick="window.location='checkout.php';" class="btn btn-danger btn-lg">Checkout</button>
		          <button type="button" onclick="window.location='buidaCistella.php';" class="btn btn-danger btn-lg">Buida cistella</button>
            </div>
        </div>
    </div>
</div>
<script>
function show() {
    var myModal = new bootstrap.Modal(document.getElementById('myModal'),{})
    myModal.show();
}
</script>


