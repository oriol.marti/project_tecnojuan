<?php
function validarLlargada($lValor){
   if( strlen($lValor) >= 3){
       return true;
   }else{
      return false;
   }
}
function validarNoBuit($valor){
   if(trim($valor) == ''){
      return false;
   }else{
      return true;
   }
}
function validarEmail($valor){
   if(filter_var($valor, FILTER_VALIDATE_EMAIL) === FALSE){
      return false;
   }else{
      return true;
   }
}
