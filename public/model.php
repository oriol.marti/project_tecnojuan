<?php

class Producte {

public $id;
static $contador;
public $titol;
public $categoria;
public $preu;
public $descripcio;
public $color;
public $stoc;
public $caracteristiques;
public $especificacions;
public $instruccions;
public $quantitat;
public $foto1;
public $foto2;
public $foto3;
public $foto4;
public $foto5;
public $foto6;
public $foto7;
public $fotos = [];


function __construct(){

    self::$contador++;
    //$this->id = self::$contador;
}

function getId(){
    return $this->id;
} 

function addFoto($fotos){

    $this->fotos[] = $fotos;
}

}

class Cataleg {

    public $productesCataleg = [];

    function addProducte($lProducte) {
        array_push($this->productesCataleg, $lProducte); 
    }

    function getProducteById($lId){
        //recorrer array prodcataleg, si $producte-id == $lId, retorna prod
        foreach($this->productesCataleg as $producte){
            if($producte->id == $lId){
                return $producte;
            }
        };
    }
    
     
    function llistar(){
        //print_r($this->productesCataleg);

        foreach($this->productesCataleg as $prod){
            echo "
            <div id='templatemo_container'>
            <div id='templatemo_content'>
                <div id='templatemo_center_section'>
                    <div class='new'>
                        <div class='product'>
                            <img style='width: 300px' alt='IMG_PROD' src='" . $prod->fotos[1] . "'<div class='product_text'> 
                            <h2>" . $prod->titol  . "</h2> <p>" . $prod->descripcio . "</p> 
                            <span class='price'>" . $prod->preu . "$</span> <span class='detail'>
                            <a href='#'>Detalles producto</a></span>
                        </div>
                    </div>
                </div>
            </div>";
        }   
    }
}

class Cistella {

    public $productesCistella = [];

    public function llistar(){

        //print_r($this->productesCistella);

    }

    public function addProducte($lProducte){
        $trobat = null;
            if($lProducte->stoc > 0) {
                if(!empty($this->productesCistella)) {
                    foreach($this->productesCistella as $prod){
                        if ($prod->id == $lProducte->id) {
                            $prod->quantitat++;
                            //$lProducte->stoc--;
                            $trobat = true;
                        }
                    }if($trobat != true){
                        array_push($this->productesCistella, $lProducte);
                        $lProducte->quantitat++;
                        //$lProducte->stoc--;
                    }
                    
                }else{
                    array_push($this->productesCistella, $lProducte);
                    $lProducte->quantitat++;
                    //$lProducte->stoc--;
                }     
            }
    }

    public function deleteProducte($lProducte){
        foreach($this->productesCistella as $i=>$prod){
            if ($prod->id == $lProducte->id) {
                $prod->quantitat--;
                if($prod->quantitat == 0){
                    unset($this->productesCistella[$i]);
                }
            }
        }
    }

    public function buidar() {

        unset($this->productesCistella);

        }

    public function getProducteById($lId){

        foreach($this->productesCistella as $producte){
            if($producte->id == $lId){
                //&$producte->quantitat = $lquantitat;
                return $producte;
            }
        };
    }

    public function getTotal(){
        $ltotal = [];
        foreach($this->productesCistella as $producte){
            Array_push($ltotal,$producte->preu*$producte->quantitat); 
        }
        return(array_sum($ltotal));
    }

    public function getNumProductes (){
        $total = [];
        foreach($this->productesCistella as $producte){
            Array_push($total,$producte->quantitat);
        }
        return(array_sum($total));
    }
 
}
