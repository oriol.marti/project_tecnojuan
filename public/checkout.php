<!doctype html>
<?php

if(isset($_SESSION['cistella'])){
    $laMevaCistella = unserialize($_SESSION['cistella']);
}
/* ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); */
require_once 'model.php';
require_once 'cataleg.php';
require_once 'validadors.php';
session_start();
$nom = isset($_REQUEST['nom']) ? $_REQUEST['nom'] : null;
$cognoms = isset($_REQUEST['cognoms']) ? $_REQUEST['cognoms'] : null;
$email = isset($_REQUEST['email']) ? $_REQUEST['email'] : null;
$adresa = isset($_REQUEST['adresa']) ? $_REQUEST['adresa'] : null;
$errores = array();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (!validarLlargada($nom)) {
       $errores["nom"] = 'El nom ha de tenir almeny 3 lletres.';
    }
    if (!validarLlargada($cognoms)) {
       $errores["cognoms"] = 'El cognom ha de tenir almeny 3 lletres.';
    }
    if (!validarEmail($email)) {
       $errores["email"] = 'El camp email es incorrecte.';
    }
    if (!validarLlargada($adresa)){
        $errores["adresa"] = 'L\'adreça ha de tenir almenys 3 lletres.';
    }
    $_SESSION["nom"] = $nom;
    $_SESSION["cognoms"] = $cognoms;
    $_SESSION["email"] = $email;
    $_SESSION["adresa"] = $adresa;
    if (!$errores) {
            header('Location: mail.php');
            exit;
    }
}//print_r($errores);

?>
<html>
  <head>
    <title>TecnoJuan</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Pagina web G4">
    <meta name="author" content="Oriol, Arnau, Roger">
    <meta name="apple-mobile-web-app-title" content="TecnoJuan">

    <meta property="og:title" content="A Basic HTML5 Template">
    <meta property="og:type" content="website">
    <meta property="og:url" content="url">
    <meta property="og:description" content="Home page">
    <meta property="og:image" content="image.png">

    <link rel="icon" href="url">
    <link rel="icon" href="url" type="image/svg+xml">
    <link rel="apple-touch-icon" href="/image.png">

    <link rel="stylesheet" href="styles.css">

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"> </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"> </script>
    <script src="https://use.fontawesome.com/releases/v5.7.2/css/all.css"> </script>

    <link rel="stylesheet" href="checkout.css">

</head>
<body>

<?php
require 'capcalera.php'
?>
<div class="container">
    <div class="row">
        <div class="col-md-7 mb-5">
            <div class="col-12 px-0">
                <div class="box-right">
                <form class="needs-validation" novalidate action="checkout.php" method="post">
                <p class="fw-bold">Informacio</p>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="nom" class="textmuted h8">Nom</label>
                            <input type="text" class="form-control" name="nom" id="nom" placeholder="" value="" required>
                            <?php
                            if($errores['nom']){
                                echo '<div class="text-danger">'.$errores['nom'].'</div>';
                            }else{
                                echo '<div class="invalid-feedback">El nom es obligatori</div>';
                            }
                            ?>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="lastName" class="textmuted h8">Cognoms</label>
                            <input type="text" class="form-control" name="cognoms" id="cognoms" placeholder="" value="" required>
                            <?php
                            if($errores['cognoms']){
                                echo '<div class="text-danger">'.$errores['cognoms'].'</div>';
                            }else{
                                echo '<div class="invalid-feedback">El cognom es obligatori</div>';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="email" class="textmuted h8">Correu <span class="text-muted">(obligatori)</span></label>
                        <input type="email" class="form-control" name="email" id="email" required>
                        <?php
                            if($errores['email']){
                                echo '<div class="text-danger">'.$errores['email'].'</div>';
                            }else{
                                echo '<div class="invalid-feedback">El email es obligatori</div>';
                            }
                        ?>
                    </div>

                    <div class="mb-3">
                        <label for="address" class="textmuted h8">Adreça</label>
                        <input type="text" class="form-control" id="adresa" name="adresa" placeholder="carrer " required>
                        <?php
                            if($errores['adresa']){
                                echo '<div class="text-danger">'.$errores['adresa'].'</div>';
                            }else{
                                echo '<div class="invalid-feedback">L\'adreça es obligatoria</div>';
                            }
                        ?>
                    </div>
                    <div class="row">
                        <div class="col-md-5 mb-3">
                            <label for="country" class="textmuted h8">Pais</label>
                            <select class="form-control d-block w-100 textmuted h8" id="country" name="pais" required>
                                <option class="textmuted h8" value="">Tria...</option>
                                <option class="textmuted h8">España</option>
                                <option class="textmuted h8">França</option>
                                <option class="textmuted h8">Catalunya</option>
                            </select>
                            <div class="invalid-feedback">
                                El Pais es obligatori
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="state" class="textmuted h8">Provincia</label>
                            <select class="form-control d-block w-100 textmuted h8" id="state" required name="provincia">
                                <option class="textmuted h8" value="">Tria...</option>
                                <option class="textmuted h8">Lleida</option>
                                <option class="textmuted h8">Tarragona</option>
                                <option class="textmuted h8">Barcelona</option>
                                <option class="textmuted h8">Girona</option>
                            </select>
                            <div class="invalid-feedback">
                                La provincia es obligatoria
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="zip" class="textmuted h8">Codi postal</label>
                            <input type="text" class="form-control d-block w-100 textmuted h8" id="zip" name="codi_postal" placeholder="" required>
                            <div class="invalid-feedback">
                                El codi postal es obligatori
                            </div>
                        </div>
                    </div>
                    
                    <hr class="mb-4">

                    <h4 class="mb-3">
                        <span class="text-muted" >LLISTAT</span>
                    </h4>
                    <!-- <div class="d-flex mb-2">
                        <p class="ms-auto textmuted"><span class="fas fa-times"></span></p>
                    </div> -->
                    <!-- <div class="d-flex mb-2">
                        <p class="h7">#7R4P50U1</p>
                    </div> -->
                    <!-- <div class="product"> -->
                    
                    
                    <table>
                    
                    <?php
                    
                    foreach ($laMevaCistella->productesCistella as $prod) {
                    echo "
                    <div class='d-flex justify-content-between align-items-center'>
                        <p class='textmuted'>" . $prod->titol  . "</p>
                        <p>Unidades:" . $prod->quantitat . "</p>
                        <p>Preu:" . $prod->preu . "€</p>
                        <img style='width: 125px;height: 100px' alt='IMG_PROD' src='" . $prod->foto1 . "'>
                    </div>
                        ";
                    } 
                    ?>
                    
                </table>
                <!--  </div>   -->
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="box-left">
                <div class="h8">

                    <div class="d-flex h7 mb-2">

                        <p class="ps-3 textmuted fw-bold h6 mb-0">TOTAL IMPORTE</p>
                        <p class="ms-auto"><?php echo $laMevaCistella->getTotal() . "€"; ?></p>
                        
                        
                    </div>
                    <p class="ms-3 px-2 bg-green">21% IVA por españita</p>
                </div>
                <div class="">
                    <br>
                    <p class="h7 fw-bold mb-1">Finalitza la compra</p>
                    <p class="textmuted h8 mb-2">rellena los detalles</p>
                    <div class="form">
                        <div class="row">
                            <div class="col-12">
                                <div class="card border-0"> <input class="form-control ps-5" type="text" placeholder="Numero tarjeta"> <span class="far fa-credit-card"></span> </div>
                            </div>
                            <div class="col-6"> <input class="form-control my-3" type="text" placeholder="MM/YY"> </div>
                            <div class="col-6"> <input class="form-control my-3" type="text" placeholder="cvv"> </div>
                            <p class="p-warning h8 fw-bold mb-3">SOLO TARJETA DE CREDITO</p>
                        </div>
                        <button class="button btn btn-danger btn-lg btn-block" type="submit">
                                    Finalitzar compra  
                        </button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-----------------FOOTER----------------->
<?php
require 'footer.php'
?>
<!-----------------FOOTER----------------->
</body>
</html>

<script>
    (function () {
  'use strict'

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = document.querySelectorAll('.needs-validation')

  // Loop over them and prevent submission
  Array.prototype.slice.call(forms)
    .forEach(function (form) {
      form.addEventListener('submit', function (event) {
        if (!form.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }

        form.classList.add('was-validated')
      }, false)
    })
})()
</script>