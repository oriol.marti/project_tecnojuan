<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'noticiamodel.php';
require_once '../conn/conexion.php';
require_once 'model.php';
session_start();
?>
<html>

<head>
	<title>TecnoJuan</title>
	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Pagina web G4">
	<meta name="author" content="Oriol, Arnau, Roger">
	<meta name="apple-mobile-web-app-title" content="TecnoJuan">

	<meta property="og:title" content="A Basic HTML5 Template">
	<meta property="og:type" content="website">
	<meta property="og:url" content="url">
	<meta property="og:description" content="Home page">
	<meta property="og:image" content="image.png">

	<link rel="icon" href="url">
	<link rel="icon" href="url" type="image/svg+xml">

	<link rel="stylesheet" href="styles.css">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>

<body>
	<!-----------------MENU----------------->
	<?php
	require 'capcalera.php'
	?>
	<!-----------------MENU----------------->

	<h1 style="text-align: center">Noticies</h1>
<?php

	if(isset($_SESSION['cistella'])){
		$laMevaCistella = unserialize($_SESSION['cistella']);
	}

    			$stmtd = $conn->prepare("SELECT * FROM noticies");

				$stmtd->setFetchMode(PDO::FETCH_CLASS , 'Noticia');

				//5) Executo la sentencia
				$stmtd->execute();
				//echo "Productos BDD";
				
				while($lamevanoticia = $stmtd->fetch()){
                    echo "
                    <div id='templatemo_content'>
                        <div id='templatemo_center_section'>
                            <div class='new'>
                                <div class='product'>
								<a style='text-decoration: none;' href='una_noticia.php?value=" . $lamevanoticia->id . "''>
                                    <div class='col-3'>
                                        <img alt='IMG_PROD' src='" . $lamevanoticia->img . "'
										<div class='product_text'> 
                                    </div>
                                    <div class='col-12'>
                                        <h2>" . $lamevanoticia->title  . "</h2>
                                        <p>" . $lamevanoticia->resumen . "</p>
                                        <a>" . $lamevanoticia->autor . "</a>
                                        <a>" . $lamevanoticia->date . "</a>
                                        <br> <br> <br> <br>
                                    </div>
								</a>	
                                </div>
                            </div>
                        </div>
                    </div>";
                } 
    ?> 

	<main>
	</main>
	<br />&nbsp;
	<!-----------------FOOTER----------------->
	<?php
	require 'footer.php'
	?>
	<!-----------------FOOTER----------------->
</body>

</html>
