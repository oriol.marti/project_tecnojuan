<?php
//$_SESSION["$laMevaCistella"];
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'model.php';
require_once 'noticiamodel.php';
require_once 'cataleg.php';
session_start();
?>

<!doctype html>
<html>
<head>
    <title>TecnoJuan</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Pagina web G4">
    <meta name="author" content="Oriol, Arnau, Roger">
    <meta name="apple-mobile-web-app-title" content="TecnoJuan">

    <meta property="og:title" content="A Basic HTML5 Template">
    <meta property="og:type" content="website">
    <meta property="og:url" content="url">
    <meta property="og:description" content="Home page">
    <meta property="og:image" content="image.png">

    <link rel="icon" href="url">
    <link rel="icon" href="url" type="image/svg+xml">
    <link rel="apple-touch-icon" href="/image.png">

    <link rel="stylesheet" href="styles.css">

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    
  </head>

  <body>
<!-----------------MENU----------------->
    <?php
    require 'capcalera.php'
    ?>
<!-----------------MENU----------------->


    <div class="header">
      <div class="text-box">
        <h1 class="heading-primary">
            <img class="logo_name" style="width: 300px; height: 300px; padding: 5%;" src="imagenes/logo_size.jpg" alt="LOGOTECNOJUAN">

		  <br>
          <span class="heading-primary-sub"><h3>TecnoJuan es una tienda de tecnologia e informatica situada en Trinitat nova que lleva más de 15 años en el barrio.</h3></span>
		  <span class="heading-primary-sub"><h3>Vendemos todo tipode gadgets tecnologicos, componentes informaticos, perifericos y mucho más!!</h3></span>
		  <span class="heading-primary-sub">Aqui encontrara una lista con una gran cantidad de los productos que ofrecemos en TecnoJuan.</span>
		</h1>
        <a href="productos.php" class="btn btn-white btn-animated" >Ver productos</a>
      </div>
    </div>


<!-----------------PRODUCTOS----------------->
<h2 id="noticias-header">Productes D'INTERÈS</h2><br>
<main>
<?php
require_once '../conn/conexion.php';
$laMevaSentencia = $conn->prepare("SELECT * FROM productes");

	$laMevaSentencia->setFetchMode(PDO::FETCH_CLASS , 'Producte');

	//5) Executo la sentencia
	$laMevaSentencia->execute();
	//echo "Productos BDD";
	//6) Itero per sobre cadascuns del gosos i els faig bordar
    $cont=0;
    while($prod = $laMevaSentencia->fetch()){
        $cont++;
        if($cont<=5){
        echo'
            <figure class="card">
                <a href="un_producte.php?value=' . $prod->id . '">
                <img class="mt-2" src="'.$prod->foto1.'" alt="iphone" style="width:100%">
                <h2 style="color:black;">'.$prod->titol.'</h2>
                <p>'.$prod->descripcio.'</p>
                <div>
                    <p>'.$prod->categoria.'</p></a>
                    <p style="color: red;">★★★★★</p>
                    <p><button><p class="price">'.$prod->preu.'€</p></button></p>
                </div>
            </figure>';
        }else{
            break;
        }
    }
    ?>
</main>





<!-----------------PRODUCTOS----------------->

	<h2 id="noticias-header">NOTICIAS D'INTERÈS</h2>
    <div class="grid-container">
        <?php
require_once '../conn/conexion.php';
$laMevaSentencia = $conn->prepare("SELECT * FROM noticies");

	$laMevaSentencia->setFetchMode(PDO::FETCH_CLASS , 'Noticia');

	//5) Executo la sentencia
	$laMevaSentencia->execute();
	//echo "Productos BDD";
	//6) Itero per sobre cadascuns del gosos i els faig bordar
    $cont=0;
    while($lamevanoticia = $laMevaSentencia->fetch()){
        $cont++;
        if($cont<=3){
        echo'
        <div class="grid-item" align="center">
            <a style="text-decoration: none;" href="una_noticia.php?value=' . $lamevanoticia->id . '"">
			    <img id="img-news" style="width: 300px;height: 300px;" src="'.$lamevanoticia->img.'" alt="IMGNOT1">
            
                <h5>'.$lamevanoticia->title.'</h5>
                <p id="text-noticia">'.$lamevanoticia->resumen.'</p>
                <a id="autor" >'. $lamevanoticia->autor .'</a> <a id="fecha">'. $lamevanoticia->date .'</a>
            </a>

        </div>';
        }else{
            break;
        }
    }
    ?>
	</div>
	<!-----------------FOOTER----------------->
	<?php
	require 'footer.php'
	?>
	<!-----------------FOOTER----------------->
  </body>
  </html>
