<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'model.php';
require_once 'cataleg.php';
ob_start();
session_start();

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
//1) Importem la bibioteca
require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';
//3) Creen una instància, amb el par`á`metre true per a habilitar les excepcions
$mail = new PHPMailer(true);
try {
   //4) Configuració del servidor SMTP
   $mail->SMTPDebug =1;                                        //Enable verbose debug output
   $mail->isSMTP();                                            //Send using SMTP
   $mail->Host       = 'smtp-oriolmarti7e5.alwaysdata.net';        //Set the SMTP server to send through
   $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
   $mail->Username   = 'oriolmarti7e5@alwaysdata.net';          //SMTP username
   $mail->Password   = '10cartaginesos';                        //SMTP password
   $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
   $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

   //Configuració del mail
   $mail->setFrom('oriolmarti7e5@alwaysdata.net', 'Tecnojuan');
   $mail->addAddress($_SESSION["email"], $_SESSION["nom"]);     //Add a recipient
   //$mail->addAddress('ellen@example.com');                    //Name is optional
   $mail->addReplyTo('oriolmarti7e5@alwaysdata.net', 'Tecnojuan');
   //$mail->addCC('cc@example.com');
   //$mail->addBCC('bcc@example.com');

   //$mail->addAttachment('/imagenes/logo_size_invert.jpg');            
   //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');         

  

    //6) Afegim el contingut del correu
    $mail->isHTML(true);                                          //Habilitar formathtml
    $mail->Subject = 'Confirmacio de compra';
    $mail->Body    = "
    <html>
    <head></head>
    <body>
        <h3>Tecnojuan</h3>
        <h3>El seu encàrrec:</h3>
        <table style='border-collapse:collapse';> 
            <thead>
                <tr>
                    <th>Nom producte</th>
                    <th>Quantitat</th>
                    <th>Preu</th>
                </tr>    
            </thead>
            <tbody>";
               $laMevaCistella = unserialize($_SESSION['cistella']);
               foreach ($laMevaCistella->productesCistella as $prod) {
                    $mail->Body .="<tr>
                        <td>" . $prod->titol  . "</td>
                        <td>" . $prod->quantitat . "</td >
                        <td>" . $prod->preu . "€</td>
                     </tr>";
               }
            $mail->Body .= "</tbody>
        </table>
        <hr />     
    </body>
    </html>";
    //$mail->AltBody = 'Aquest es el contingut del correu es pot formatejar am html pero no tots el lectors de correo ho permeten ';
 
    $mail->send();
   echo 'El missatge s \'ha enviat ';
} catch (Exception $e) {
   echo "Error al enviar el correu: {$mail->ErrorInfo}";
}

$mail2 = new PHPMailer(true);
try {
   //4) Configuració del servidor SMTP
   $mail2->SMTPDebug =1;                                        //Enable verbose debug output
   $mail2->isSMTP();                                            //Send using SMTP
   $mail2->Host       = 'smtp-oriolmarti7e5.alwaysdata.net';        //Set the SMTP server to send through
   $mail2->SMTPAuth   = true;                                   //Enable SMTP authentication
   $mail2->Username   = 'oriolmarti7e5@alwaysdata.net';          //SMTP username
   $mail2->Password   = '10cartaginesos';                        //SMTP password
   $mail2->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
   $mail2->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

   //Configuració del mail
   $mail2->setFrom('oriolmarti7e5@alwaysdata.net', 'Tecnojuan');
   $mail2->addAddress('oriol.marti.7e5@itb.cat', 'Oriol');     //Add a recipient
   //$mail2->addAddress('ellen@example.com');                    //Name is optional
   $mail2->addReplyTo('oriolmarti7e5@alwaysdata.net', 'Tecnojuan');
   //$mail2->addCC('cc@example.com');
   //$mail2->addBCC('bcc@example.com');

   //$mail2->addAttachment('/imagenes/logo_size_invert.jpg');            
   //$mail2->addAttachment('/tmp/image.jpg', 'new.jpg');         

  

    //6) Afegim el contingut del correu
    $mail2->isHTML(true);                                          //Habilitar formathtml
    $mail2->Subject = 'Detalls Compra';
    $mail2->Body    = "
    <html>
    <head></head>
    <body>
        <h3>Tecnojuan</h3>
        <h3>Dades del client: </h3>
            <p>Nom: ". $_SESSION['nom']."</p>
            <p>Cognoms: ".$_SESSION['cognoms']."</p>
            <p>Adreça: ".$_SESSION['adresa']." </p>
            <p>Email: ".$_SESSION['email']."</p>
        <h3>Encàrrec del client:</h3>
        <table style='border-collapse:collapse';> 
            <thead>
                <tr>
                    <th>Nom producte</th>
                    <th>Quantitat</th>
                    <th>Preu</th>
                </tr>    
            </thead>
            <tbody>";
               $laMevaCistella = unserialize($_SESSION['cistella']);
               foreach ($laMevaCistella->productesCistella as $prod) {
                    $mail2->Body .="<tr>
                        <td>" . $prod->titol  . "</td>
                        <td>" . $prod->quantitat . "</td >
                        <td>" . $prod->preu . "€</td>
                     </tr>";
               }
            $mail2->Body .= "</tbody>
        </table>
        <hr />     
    </body>
    </html>";
    //$mail2->AltBody = 'Aquest es el contingut del correu es pot formatejar am html pero no tots el lectors de correo ho permeten ';
 
    $mail2->send();
   echo 'El missatge s \'ha enviat ';
} catch (Exception $e) {
   echo "Error al enviar el correu: {$mail2->ErrorInfo}";
}


unset($_SESSION['cistella']);
header('Location: index.php');

?>