<!doctype html>
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'model.php';
require_once 'noticiamodel.php';
require_once 'cataleg.php';
session_start();
	?>
<html>


<head>
	<title>TecnoJuan</title>
	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Pagina web G4">
	<meta name="author" content="Oriol, Arnau, Roger">
	<meta name="apple-mobile-web-app-title" content="TecnoJuan">

	<meta property="og:title" content="A Basic HTML5 Template">
	<meta property="og:type" content="website">
	<meta property="og:url" content="url">
	<meta property="og:description" content="Home page">
	<meta property="og:image" content="image.png">

	<link rel="icon" href="url">
	<link rel="icon" href="url" type="image/svg+xml">
	<link rel="apple-touch-icon" href="/image.png">

	<link rel="stylesheet" href="styles.css">
	<link rel="stylesheet" href="unanoticia.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>

<body>
	<!-----------------MENU----------------->
	<?php
	require 'capcalera.php';
	require_once '../conn/conexion.php';
	?>
	<!-----------------MENU----------------->


	<!-----------------PRODUCTO----------------->
			<?php
			$laMevaSentencia = $conn->prepare("SELECT * FROM noticies");

			$laMevaSentencia->setFetchMode(PDO::FETCH_CLASS , 'Noticia');

			//5) Executo la sentencia
			$laMevaSentencia->execute();
			//echo "Productos BDD";
			//6) Itero per sobre cadascuns del gosos i els faig bordar
			while($lamevanoticia = $laMevaSentencia->fetch()){
                
				if ($lamevanoticia->id == $_GET['value']) {
					echo '
                <article class="noticiaaaa">
                <h1>'.$lamevanoticia->title.'</h1>
                <img src="'.$lamevanoticia->img.'" alt="foto-noticia" width="95%"/><br><br>
                <p>'.$lamevanoticia->info.'</p>
                <cite>
                by <span class="name">'. $lamevanoticia->autor .'</span> | 
                Date: <span class="name">'. $lamevanoticia->date .'</span>
                </cite>
                
                </article>';
                
				}
			}
			?>


	<!-----------------PRODUCTO----------------->

	<!-----------------FOOTER----------------->
	<?php
	require 'footer.php';
	?>
	<!-----------------FOOTER----------------->
</body>

</html>