<!doctype html>
<?php

if(isset($_SESSION['cistella'])){
    $laMevaCistella = unserialize($_SESSION['cistella']);
}
/* ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); */
require_once 'model.php';
require_once 'cataleg.php';
require_once 'validadors.php';
session_start();
$nom = isset($_REQUEST['nom']) ? $_REQUEST['nom'] : null;
$cognoms = isset($_REQUEST['cognoms']) ? $_REQUEST['cognoms'] : null;
$email = isset($_REQUEST['email']) ? $_REQUEST['email'] : null;
$adresa = isset($_REQUEST['adresa']) ? $_REQUEST['adresa'] : null;
$errores = array();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (!validarLlargada($nom)) {
       $errores["nom"] = 'El nom ha de tenir almeny 3 lletres.';
    }
    if (!validarLlargada($cognoms)) {
       $errores["cognoms"] = 'El cognom ha de tenir almeny 3 lletres.';
    }
    if (!validarEmail($email)) {
       $errores["email"] = 'El camp email es incorrecte.';
    }
    if (!validarLlargada($adresa)){
        $errores["adresa"] = 'L\'adreça ha de tenir almenys 3 lletres.';
    }
    $_SESSION["nom"] = $nom;
    $_SESSION["cognoms"] = $cognoms;
    $_SESSION["email"] = $email;
    $_SESSION["adresa"] = $adresa;
    if (!$errores) {
            header('Location: mail.php');
            exit;
    }
}//print_r($errores);

?>
<html>
    <head>
        <title>TecnoJuan</title>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Pagina web G4">
        <meta name="author" content="Oriol, Arnau, Roger">
        <meta name="apple-mobile-web-app-title" content="TecnoJuan">

        <meta property="og:title" content="A Basic HTML5 Template">
        <meta property="og:type" content="website">
        <meta property="og:url" content="url">
        <meta property="og:description" content="Home page">
        <meta property="og:image" content="image.png">

        <link rel="icon" href="url">
        <link rel="icon" href="url" type="image/svg+xml">
        <link rel="apple-touch-icon" href="/image.png">

        <link rel="stylesheet" href="styles.css">

        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"> </script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"> </script>
        <script src="https://use.fontawesome.com/releases/v5.7.2/css/all.css"> </script>

        <link rel="stylesheet" href="checkout.css">

    </head>
    <body>
        <?php
        require 'capcalera.php'
        ?>
        <div class="container">
            <form class="needs-validation" novalidate action="javascript:alert('Hem rebut el teu correu!')" method="post">
                <p class="fw-bold">Contacte</p>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="nom" class="textmuted h8">Nom</label>
                            <input type="text" class="form-control" name="nom" id="nom" placeholder="" value="" required>
                            <?php
                            if($errores['nom']){
                                echo '<div class="text-danger">'.$errores['nom'].'</div>';
                            }else{
                                echo '<div class="invalid-feedback">El nom es obligatori</div>';
                            }
                            ?>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="lastName" class="textmuted h8">Cognoms</label>
                            <input type="text" class="form-control" name="cognoms" id="cognoms" placeholder="" value="" required>
                            <?php
                            if($errores['cognoms']){
                                echo '<div class="text-danger">'.$errores['cognoms'].'</div>';
                            }else{
                                echo '<div class="invalid-feedback">El cognom es obligatori</div>';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="email" class="textmuted h8">Correu <span class="text-muted">(obligatori)</span></label>
                        <input type="email" class="form-control" name="email" id="email" required>
                        <?php
                            if($errores['email']){
                                echo '<div class="text-danger">'.$errores['email'].'</div>';
                            }else{
                                echo '<div class="invalid-feedback">El email es obligatori</div>';
                            }
                        ?>
                    </div>
                    <div class="mb-3">
                        <label for="address" class="textmuted h8">Envian's els teus comentaris</label>
                        <textarea style="height: 500px;" class="form-control" name="comentaris" required></textarea>
                    </div>
                        <div>
                            <button type="submit" class="btn btn-danger">Envia</button>
                        </div>
                    </div><br>
            </form>
        </div>
    <!-----------------FOOTER----------------->
	<?php
	require 'footer.php'
	?>
	<!-----------------FOOTER----------------->
    </body>
</html>