<?php
//$_SESSION["$laMevaCistella"];
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'model.php';
require_once 'cataleg.php';
session_start();
?>
<html>

<head>
	<title>TecnoJuan</title>
	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Pagina web G4">
	<meta name="author" content="Oriol, Arnau, Roger">
	<meta name="apple-mobile-web-app-title" content="TecnoJuan">

	<meta property="og:title" content="A Basic HTML5 Template">
	<meta property="og:type" content="website">
	<meta property="og:url" content="url">
	<meta property="og:description" content="Home page">
	<meta property="og:image" content="image.png">

	<link rel="icon" href="url">
	<link rel="icon" href="url" type="image/svg+xml">
	<link rel="apple-touch-icon" href="/image.png">

	<link rel="stylesheet" href="styles.css">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>

<body>
	<!-----------------MENU----------------->
	<?php
	require 'capcalera.php';
	require_once '../conn/conexion.php';
	?>
	<!-----------------MENU----------------->

	<h1 style="text-align: center">Productos</h1>
	<?php

	if(isset($_SESSION['cistella'])){
		$laMevaCistella = unserialize($_SESSION['cistella']);
	}
	
	$laMevaSentencia = $conn->prepare("SELECT * FROM productes");

	$laMevaSentencia->setFetchMode(PDO::FETCH_CLASS , 'Producte');

	//5) Executo la sentencia
	$laMevaSentencia->execute();
	//echo "Productos BDD";
	//6) Itero per sobre cadascuns del gosos i els faig bordar
	while($prod = $laMevaSentencia->fetch()){

		echo "
		<div id='templatemo_content'>
			<div id='templatemo_center_section'>
				<div class='new'>
					<div class='product d-block'>
						<div class='col-3'>
						<a href='un_producte.php?value=" . $prod->id . "'>
							<img class='product_text' alt='IMG_PROD' src='" . $prod->foto1 . "'/>
						</div>
						<div class='row'>
							<div class='col-12'>
								<h2>" . $prod->titol  . "</h2>
								<p>" . $prod->descripcio . "</p>
							</div>
						</div>
						</a>
						<div class='row'>
							<div class='col-12 d-flex justify-content-end'>
								<span class='price'>" . $prod->preu . "€</span>
								<form class='m-0 align-self-center' method='POST' action='afegirProducte.php'>
									<input type='hidden' name='idproducte' value='" . $prod->id . "'>
									<button class='btn btn-danger btn-animated' type='submit'>Añadir al carro</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>";
	}
	$_SESSION['cistella'] = serialize($laMevaCistella);
	?>
	<main>
	</main>
	<br />&nbsp;
	<!-----------------FOOTER----------------->
	<?php
	require 'footer.php'
	?>
	<!-----------------FOOTER----------------->
</body>

</html>