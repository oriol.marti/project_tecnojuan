<footer>
		<div class="content">
			<div class="top">
				<div class="logo-details">
					<img class="logo_footer" style="width: 150px;height: 150px;" src="imagenes/logo_size_invert.jpg" alt="LOGOTECNOJUAN">
				</div>
				<div>
					<a href="#"><img style="width: 80px;height: 50px;" src="imagenes/insta.png" alt="LOGOINSTA"></a>
					<a href="#"><img style="width: 55px;height: 50px; padding-right: 15px;" src="imagenes/Facebook.png" alt="LOGOINSTA"></i></a>
					<a href="#"><img style="width: 60px;height: 50px; padding-right: 15px;" src="imagenes/twitter.png" alt="LOGOINSTA"></i></a>
					<a href="#"><img style="width: 45px;height: 50px;" src="imagenes/linkedin.png" alt="LOGOINSTA"></i></a>
				</div>
			</div>
			<div class="link-boxes">
				<ul class="box">
					<li class="link_name">Links</li>
					<li><a href="#">Home</a></li>
					<li><a href="#">Contacto</a></li>
					<li><a href="#">Donde encontrarnos</a></li>
					<li><a href="#">Noticias</a></li>

				</ul>
				<ul class="box">
					<li class="link_name">Productos</li>
					<li><a href="#">Tecnologia</a></li>
					<li><a href="#">Componenetes</a></li>
					<li><a href="#">Perifericos</a></li>
					<li><a href="#">Cables</a></li>

				</ul>
				<ul class="box">
					<li class="link_name">Other services</li>
					<li><a href="#">SEO</a></li>
					<li><a href="#">Content Marketing</a></li>
					<li><a href="#">Prints</a></li>
					<li><a href="#">Social Media</a></li>

				</ul>
				<ul class="box">
					<li class="link_name">Contacto</li>
					<li><a href="#">+99999999999</a></li>
					<li><a href="#">+99999999999</a></li>
				</ul>
				<div class="iframeGM">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2076.2631850363146!2d2.1838591875782134!3d41.45432611036356!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4bcfdad2100a9%3A0x3c4e7db0a61a83e0!2sInstitut%20Tecnol%C3%B2gic%20de%20Barcelona!5e0!3m2!1sca!2ses!4v1634910062009!5m2!1sca!2ses" width="200" height="200" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
				</div>
			</div>
		</div>
		<div class="bottom-details">
			<div class="bottom_text">
				<span class="copyright_text">Copyright © 2021 <a href="#">TecnoJuan.</a></span>
				<span class="policy_terms">
					<a href="#">Politicas de privacidad</a>

				</span>
			</div>
		</div>
	</footer>