<!doctype html>
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'model.php';
require_once 'cataleg.php';
session_start();
	?>
<html>


<head>
	<title>TecnoJuan</title>
	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Pagina web G4">
	<meta name="author" content="Oriol, Arnau, Roger">
	<meta name="apple-mobile-web-app-title" content="TecnoJuan">

	<meta property="og:title" content="A Basic HTML5 Template">
	<meta property="og:type" content="website">
	<meta property="og:url" content="url">
	<meta property="og:description" content="Home page">
	<meta property="og:image" content="image.png">

	<link rel="icon" href="url">
	<link rel="icon" href="url" type="image/svg+xml">
	<link rel="apple-touch-icon" href="/image.png">

	<link rel="stylesheet" href="styles.css">
	<link rel="stylesheet" href="un_producte.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <script>
        var actual_page = 0
        
        function nextPage(){
            var div = document.querySelector("#images > div")
            console.log(div)
            if (actual_page >= total_pages) {
                div.style.transform="translateX(-"+(total_pages*100)+"%)"
            } else {
                actual_page++;
                div.style.transform="translateX(-"+(actual_page*100)+"%)"
            }
        console.log(actual_page)
        console.log(total_pages)
        }

        function prevPage(){
            var div = document.querySelector("#images > div")
            console.log(div)
            if (actual_page > 0) {
                actual_page--;
                div.style.transform="translateX(-"+(actual_page*100)+"%)"
            } else {
                div.style.transform="translateX(0%)"
            }
        }
    </script>
</head>

<body>
	<!-----------------MENU----------------->
	<?php
	require 'capcalera.php';
	require_once '../conn/conexion.php';
	?>
	<!-----------------MENU----------------->


	<!-----------------PRODUCTO----------------->
			
	<h1 style="text-align: center">Detalles del producto</h1>
			<?php
			$laMevaSentencia = $conn->prepare("SELECT * FROM productes");

			$laMevaSentencia->setFetchMode(PDO::FETCH_CLASS , 'Producte');

			$laMevaSentencia->execute();
			while($prod = $laMevaSentencia->fetch()){
				$cont = 0;
				$arrayFotos = [];
				foreach($prod as $cosa){
					$cont++;
					if($cont == 12||$cont == 13||$cont == 14||$cont == 15||$cont == 16||$cont == 17||$cont == 18){
						if(!empty($cosa)){
							array_push($arrayFotos, $cosa);
						}
					}
				}
				if ($prod->id == $_GET['value']) {
					echo "	<div class='jumbotron'>
                                <div class='primer_div'>
									<div class='segundo_div'>
										<div id='images'>
											<div>";
											$total_fotos = count($arrayFotos);
											for($i = 0; $i<=$total_fotos-1; $i++){
												echo "<img class='images' alt='IMG_PROD' src='" . $arrayFotos[$i] . "'>";
											} ?>	
											<script>
												var total_pages = <?= $total_fotos-1 ?>
											</script>
											<?php
											echo "</div>
                							<button onclick='prevPage()' class='button_img'>&lt;</button>
                							<button onclick='nextPage()' style='right: .1em;' class='button_img'>&gt;</button>
            							</div>
										<div class='tercer_div'>
											<h2>" . $prod->titol  . "</h2>
											<h3>" . $prod->categoria . "</h3>
											<p class='descripcio'>" . $prod->descripcio . "</p> <br>
											<p class='precio'>" . $prod->preu . "€</p>    
											<form method='POST' action='afegirProducte.php'>
												<input type='hidden' name='idproducte' value='" . $prod->id . "'>
												<button type='submit' id='button_buy' class='btn btn-danger btn-animated ml-5px'>Añadir al carrito</button>
											</form>
										</div>
									</div>
								</div>
							<div>";
				}
			}
			?>

	<!-----------------PRODUCTO----------------->

	<!-----------------FOOTER----------------->
	<?php
	require 'footer.php';
	?>
	<!-----------------FOOTER----------------->
</body>

</html>