<?php 
//0) activo els errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//1) Activo la sessió
session_start();

//2/ si la variable de sessió no esta establerta reridigeix a auteti.php

if( !isset($_SESSION["usuari"]) ){
    header('Location: autenti.html'  );    
}

require_once '../conn/conexion.php';
require_once '../public/model.php';
//require_once 'cataleg.php';
?>


    <!DOCTYPE html>
    <html lang="en">

    <head>
    <meta charset="UTF-8">
    <title>Panel administracion</title>
    <meta charset="utf-8" />
	<meta name="viewport" content="initial-scale=1.0; maximum-scale=1.0; width=device-width;">
	<link rel="stylesheet" href="./stylesheets/screen.css">
	<link rel="stylesheet" href="./stylesheets/fonts.css">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/releases/v5.7.2/css/all.css"> </script>
    </head>


    <body>
    <section class="blank">
		<nav class="navigation">
			<ul>
				<li>
				<a href="#" class="navigation-user">
						<i class="icon-large icon-user"></i>
					</a>
				</li>
				<li>
					<a href="#"><?php echo $_SESSION["usuari"];?></a>
				</li>
                <li>
					<a href="pagina1.php">Tornar</a>
				</li>
				<li>
					<form action="login.php">
					<input type="hidden" name="accio" value="sortir" />
					<button type="submit">Sortir</button>
					</form>
				</li>
			</ul>
			<br class="clear">
		</nav>
	</section>

    <?php
    $laMevaSentencia = $conn->prepare("SELECT * FROM productes");

    $laMevaSentencia->setFetchMode(PDO::FETCH_CLASS , 'Producte');

    //5) Executo la sentencia
    $laMevaSentencia->execute();
    while($prod = $laMevaSentencia->fetch()){
        if ($prod->id == $_REQUEST['idproducte']) {
    ?>
    <section>
        <div class="container">
            <form action="update.php">
                <input type="hidden" name="idProducte" value="<?= $prod->id ?>">
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="nom" class="textmuted h8">Titol</label>
                    <input type="text" class="form-control" name="titol" id="titol" value="<?= htmlentities($prod->titol) ?>" required>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="lastName" class="textmuted h8">Categoria</label>
                    <input type="text" class="form-control" name="categoria" id="categoria" value="<?= htmlentities($prod->categoria) ?>" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="nom" class="textmuted h8">preu</label>
                    <input type="text" class="form-control" name="preu" id="preu" value="<?= htmlentities($prod->preu) ?>" required>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="lastName" class="textmuted h8">descripcio</label>
                    <input type="text" class="form-control" name="descripcio" id="descripcio" maxlength="512" value="<?= htmlentities($prod->descripcio) ?>" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="nom" class="textmuted h8">colors</label>
                    <input type="text" class="form-control" name="colors" id="colors" value="<?= htmlentities($prod->colors) ?>" required>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="lastName" class="textmuted h8">stoc</label>
                    <input type="text" class="form-control" name="stoc" id="stoc" value="<?= htmlentities($prod->stoc) ?>" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="nom" class="textmuted h8">caracteristiques</label>
                    <input type="text" class="form-control" name="caracteristiques" id="caracteristiques" value="<?= htmlentities($prod->caracteristiques) ?>" required>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="lastName" class="textmuted h8">especificacions</label>
                    <input type="text" class="form-control" name="especificacions" id="especificacions" value="<?= htmlentities($prod->especificacions) ?>" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="nom" class="textmuted h8">instruccions</label>
                    <input type="text" class="form-control" name="instruccions" id="instruccions" value="<?= htmlentities($prod->instruccions) ?>" required>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="nom" class="textmuted h8">foto1</label>
                    <input type="text" class="form-control" name="foto1" id="foto1" value="<?= htmlentities($prod->foto1) ?>" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="nom" class="textmuted h8">foto2</label>
                    <input type="text" class="form-control" name="foto2" id="foto2" value="<?= htmlentities($prod->foto2) ?>" required>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="nom" class="textmuted h8">foto3</label>
                    <input type="text" class="form-control" name="foto3" id="foto3" value="<?= htmlentities($prod->foto3) ?>" required>
                </div>
            </div>
            <button type="submit" class="btn btn-danger">Submit</button>
            </form>
        </div>
    </section>
    <?php }} ?>
</body>