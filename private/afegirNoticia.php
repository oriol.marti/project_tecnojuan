<?php 
//0) activo els errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//1) Activo la sessió
session_start();

//2/ si la variable de sessió no esta establerta reridigeix a auteti.php

if( !isset($_SESSION["usuari"]) ){
    header('Location: autenti.html'  );    
}

require_once '../conn/conexion.php';
require_once '../public/model.php';
//require_once 'cataleg.php';
?>


    <!DOCTYPE html>
    <html lang="en">

    <head>
    <meta charset="UTF-8">
    <title>Panel administracion</title>
    <meta charset="utf-8" />
	<meta name="viewport" content="initial-scale=1.0; maximum-scale=1.0; width=device-width;">
	<link rel="stylesheet" href="./stylesheets/screen.css">
	<link rel="stylesheet" href="./stylesheets/fonts.css">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/releases/v5.7.2/css/all.css"> </script>
    </head>


    <body>
    <section class="blank">
		<nav class="navigation">
			<ul>
				<li>
				<a href="#" class="navigation-user">
						<i class="icon-large icon-user"></i>
					</a>
				</li>
				<li>
					<a href="#"><?php echo $_SESSION["usuari"];?></a>
				</li>
                <li>
					<a href="pagina2.php">Tornar</a>
				</li>
				<li>
					<form action="login.php">
					<input type="hidden" name="accio" value="sortir" />
					<button type="submit">Sortir</button>
					</form>
				</li>
			</ul>
			<br class="clear">
		</nav>
	</section>
    <section>
        <div class="container">
            <form action="insertNoticia.php">
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="nom" class="textmuted h8">Titol</label>
                    <input type="text" class="form-control" name="title" id="title" placeholder="" value="" required>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="lastName" class="textmuted h8">Autor</label>
                    <input type="text" class="form-control" name="autor" id="autor" placeholder="" value="" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="nom" class="textmuted h8">Data</label>
                    <input type="text" class="form-control" name="date" id="date" placeholder="" value="" required>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="lastName" class="textmuted h8">Resumen</label>
                    <input type="text" class="form-control" name="resumen" id="resumen" placeholder="" value="" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="nom" class="textmuted h8">Info</label>
                    <input type="text" class="form-control" name="info" id="info" placeholder="" value="" required>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="nom" class="textmuted h8">foto1</label>
                    <input type="text" class="form-control" name="foto1" id="foto1" placeholder="" value="" required>
                </div>
            </div>
            <button type="submit" class="btn btn-danger">Submit</button>
            </form>
        </div>
    </section>
</body>