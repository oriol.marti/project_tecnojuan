<?php 
//0) activo els errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//1) Activo la sessió
session_start();

//2/ si la variable de sessió no esta establerta reridigeix a auteti.php

if( !isset($_SESSION["usuari"]) ){
    header('Location: autenti.html'  );    
}

require_once '../conn/conexion.php';
require_once '../public/model.php';
//require_once 'cataleg.php';
?>


    <!DOCTYPE html>
    <html lang="en">

    <head>
    <meta charset="UTF-8">
    <title>Panel administracion</title>
    <meta charset="utf-8" />
	<meta name="viewport" content="initial-scale=1.0; maximum-scale=1.0; width=device-width;">
	<link rel="stylesheet" href="./stylesheets/screen.css">
	<link rel="stylesheet" href="./stylesheets/fonts.css">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    </head>

    <body>
	<section class="blank">
		<nav class="navigation">
			<ul>
			<li>
				<a href="pagina1.php" class="navigation-user">
						<i class="">Productes</i>
					</a>
				</li>
                <li>
				<a href="pagina2.php" class="navigation-user">
						<i class="">Noticies</i>
					</a>
				</li>
				<li>
				<a href="#" class="navigation-user">
						<i class="icon-large icon-user"></i>
					</a>
				</li>
				<li>
					<a href="#"><?php echo $_SESSION["usuari"];?></a>
				</li>
				<li>
					<form action="login.php">
					<input type="hidden" name="accio" value="sortir" />
					<button type="submit">Sortir</button>
					</form>
				</li>

			</ul>
			<br class="clear">
		</nav>
	</section>
	<section>	
	<a href="afegirProd.php" class="button button-submit">Crea un producto nuevo </a>
	</section>
<!-- 	<section class="blank items">
		<div class="item"> -->
				<?php
				$laMevaSentencia = $conn->prepare("SELECT * FROM productes");

				$laMevaSentencia->setFetchMode(PDO::FETCH_CLASS , 'Producte');

				//5) Executo la sentencia
				$laMevaSentencia->execute();
				//echo "Productos BDD";
				//6) Itero per sobre cadascuns del gosos i els faig bordar
				while($elmeuproducte = $laMevaSentencia->fetch()){
				echo "
				<section class='blank'>
				<div class='item-featured'>
				<img src=".$elmeuproducte->foto1." alt='Image' width='200' height='400' style='max-width: 200;'>
					<div class='item-info'>
						<h1>".$elmeuproducte->titol."</h1>
						<a>".$elmeuproducte->preu." $</a><br>
						<a> ID : ".$elmeuproducte->id." </a>
						<p>".$elmeuproducte->descripcio."</p>
						<form class='m-0 h-100 align-self-center' method='POST' action='modificador.php'>
							<input type='hidden' name='idproducte' value='" . $elmeuproducte->id . "'>
							<button class='button' type='submit'> Editor. </button>
						</form>
						<form class='m-0 h-100 align-self-center' method='POST' action='deleter.php'>
							<input type='hidden' name='deleterid' value='" . $elmeuproducte->id . "'>
							<button class='button button-delete' type='submit'>Delete</button>
						</form>
					</div>
					<br class='clear'>
				</div>
				</div>
				</div>
			</section>
				";
				}
				?>
</body>

</html>