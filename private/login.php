<?php
//0) activo els errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//1) Activo la sessió
session_start();

//1.1) miro que l'acció sisgui autenticar
if( isset($_REQUEST["accio"]) ){
    if($_REQUEST["accio"] == "autenticar"){
       
        //2) Comprovo que els parametres user i contra m'arriben
        if( testUsuariContrasenya($_REQUEST["usuari"],$_REQUEST["contrasenya"]) ){
            //3) si passa l'autentificació estableixo la cookie
            $usuari = $_REQUEST["usuari"];
            $contrasenya = $_REQUEST["contrasenya"];
            $_SESSION['usuari']  = $usuari;
            $_SESSION['constrasenya'] = $contrasenya;
            // 5)Si no passa la autentificació redirigeixo a pagina1.php
            header('Location: pagina1.php' );
        }
            else{
                // 5)Si no passa la autentificació redirigeixo a autenti.php
                header('Location: autenti.html' );
            }
    }
}


if( isset($_REQUEST["accio"]) ){
    if($_REQUEST["accio"] == "sortir"){
        session_destroy();
        header('Location: autenti.html' );
    }
}

function testUsuariContrasenya($lUsuari,$lcontrasenya){
    if( ($lUsuari == "rafa" && $lcontrasenya =="cuestas") ||
        ($lUsuari == "admin" && $lcontrasenya =="admin")
     ){
         return true;
     }
     else{
         return false;
     }
}